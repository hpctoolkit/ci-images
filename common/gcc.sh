#!/bin/bash -ex

VER="$1"
shift

# Splat the sources somewhere in /tmp
mkdir /tmp/gcc
tar -x -f /src/gcc-"$VER".tar.xz -C /tmp/gcc --strip-components=1

# Configure the build directory
mkdir /tmp/gcc-build
(cd /tmp/gcc-build && /tmp/gcc/configure \
  --quiet --enable-silent-rules \
  --prefix=/opt/gcc-"$VER" --disable-bootstrap --disable-werror \
  --enable-languages=c,c++ --disable-multilib "$@")

# Build and install to /opt
make -C /tmp/gcc-build -j16 >/dev/null 2>&1
make -C /tmp/gcc-build -j16 -O -s --no-print-directory install

# Strip all executables in /opt to reduce the size
# Ignore the return code for files mark executable but not ELF
find /opt/gcc-"$VER" -type f -executable -execdir strip '{}' + || :
strip /opt/gcc-"$VER"/bin/gcc /opt/gcc-"$VER"/bin/g++

# Add links to the compilers themselves in /usr/local/bin
ln -s /opt/gcc-"$VER"/bin/gcc /usr/local/bin/gcc-"$VER"
ln -s /opt/gcc-"$VER"/bin/g++ /usr/local/bin/g++-"$VER"

# Tarball the binaries for extraction
tar cJf /dst/gcc-"$VER".tar.xz /opt/gcc-"$VER" /usr/local/bin/gcc-"$VER" /usr/local/bin/g++-"$VER"
