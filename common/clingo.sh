#!/bin/sh -e

# Update pip before doing anything else
python3 -m pip install --upgrade pip

# Install clingo through pip
python3 -m pip install --prefix /usr/local --only-binary :all: clingo

# Uninstall the updated pip so we don't include it
python3 -m pip uninstall --yes pip

# Tarball the resulting binaries for extraction
tar cJf /dst/clingo.tar.xz /usr/local/lib*/python*
