#!/bin/bash

# Usage: $0 <output file> <URL> <SHA256>

if test -f "$1"; then
  # Verify that the cached file is correct
  if (echo "$3 $1" | sha256sum --check -); then
    exit 0
  fi
  echo "Cached file failed to verify, will redownload"
fi

# Download from the source
curl -L -o "$1" "$2" || exit $?

# Verify the newly downloaded file
echo "$3 $1" | sha256sum --check - || exit $?
