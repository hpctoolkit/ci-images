#!/usr/bin/env python3

import importlib
import sys

# We need Python 3.10 or better
if sys.version_info < (3, 10):
    raise RuntimeError(f"Python version too low, need >=3.10 but got {sys.version_info}")

# Make sure some of the optional standard modules are available
importlib.import_module("zlib")
importlib.import_module("lzma")
importlib.import_module("bz2")

if len(sys.argv) <= 1 or sys.argv[1] != "--no-extras":
    # Extended modules that we (mostly Spack) needs
    importlib.import_module("boto3")
