#!/bin/bash -ex

# Install the dependencies
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install --yes --no-install-recommends \
  gcc g++ \
  libbz2-dev \
  libdb-dev \
  libexpat1-dev \
  libffi-dev \
  libgdbm-dev \
  liblzma-dev \
  libmpdec-dev \
  libncursesw5-dev \
  libreadline-dev \
  libsqlite3-dev \
  libssl-dev \
  make \
  xz-utils \
  zlib1g-dev \
# end
